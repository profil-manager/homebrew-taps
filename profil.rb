class Profil < Formula
  desc "Profil cli"
  homepage "https://gitlab.com/profil-manager/cli/-/wikis/home"
  url "https://gitlab.com/profil-manager/cli/-/archive/v0.0.1/cli-v0.0.1.tar.gz"
  sha256 "b367075ebea39bd7e0a2bb7e9aac95935864a696fa84c3a60f2990e6e3d2db30"
  license "MIT"

  depends_on "git" => :build
  depends_on "git-crypt" => :build
  depends_on "yq" => :build
  depends_on "jq" => :build

  def install
    libexec.install %w[bin lib]
    bin.install_symlink Dir["#{libexec}/bin/*"]
  end

  test do
    system "#{bin}/profil", "--version"
  end
end
